class UserThread internal constructor(name: String,var timeOut: Int) : Thread(name) {
    override fun run() {
        Thread.currentThread().name = name
        println("User ${currentThread().name} start work")
        try {
          Thread.sleep(timeOut.toLong()*100)
        } catch (e: InterruptedException) {
            println("Thread was breaked")
        }
        println("User ${currentThread().name} end his work in $timeOut minutes")
    }
}