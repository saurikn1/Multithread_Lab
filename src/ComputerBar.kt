import java.util.*
import java.util.concurrent.Executors

class ComputerBar {
    constructor(computersCount: Int, usersCount: Int){
        startThreads(usersCount,computersCount)
    }

    private fun startThreads(usersCount: Int,computersCount: Int){
        val executorService = Executors.newFixedThreadPool(computersCount)
        for (i in 0 until usersCount) {
            executorService.submit(UserThread("" + i,randromTime()))
        }
    }

    private fun ClosedRange<Int>.random() = Random().nextInt(endInclusive - start) + start
    private fun randromTime(): Int {
        return (15..120).random()
    }
}